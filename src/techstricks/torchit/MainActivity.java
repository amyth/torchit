package techstricks.torchit;

import java.io.IOException;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity implements Callback {	
	//Variable Declarations
	private Boolean isFlashOn = false;
	SurfaceView preview;
	SurfaceHolder holder;
	Camera mycam = Camera.open();
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Check if Flash Light is Available
        Boolean has_flash = this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if(has_flash){
        	setContentView(R.layout.activity_main);
        	preview = (SurfaceView)findViewById(R.id.pSv);
        	holder = preview.getHolder();
        	holder.addCallback(this);
        	disableSleepMode();
        	screenTorchOn();
        	ToggleTorch();
        } else {
        	setContentView(R.layout.activity_main);
        	// Disable the Flash Button
        	ImageButton flashButton = (ImageButton)findViewById(R.id.tt);
        	flashButton.setImageResource(R.drawable.flash_button_disabled);
        	flashButton.setEnabled(false);
        	disableSleepMode();
        	screenTorchOn();
        	//Tell the user that no flash was detected
        	Toast.makeText(this, "No Flash LED detected. Disabling Flash Torch. You can use the Screen Brightness Torch.", Toast.LENGTH_LONG).show();
        }
    }
    
    public void initFlashLight(Camera camera){
    	Parameters myparams = camera.getParameters();
    	myparams.setFlashMode(Parameters.FLASH_MODE_TORCH);
    	myparams.setFocusMode(Parameters.FOCUS_MODE_INFINITY);
    	camera.setParameters(myparams);
    }
    
    public void ToggleTorch(){
    	final ImageButton tt = (ImageButton)findViewById(R.id.tt);
    	tt.setOnClickListener(new OnClickListener(){
    		@Override
    		public void onClick(View v){
    			if (isFlashOn){
    				mycam.stopPreview();
    				mycam.release();
    				mycam = null;
    				isFlashOn = false;
    				tt.setImageResource(R.drawable.flash_button_normal);
    			} else {
    				if (mycam == null){
    					mycam = Camera.open();
    				}
    				initFlashLight(mycam);
    				try {
    					mycam.setPreviewDisplay(holder);
    				} catch (IOException e) {
    					e.printStackTrace();
    				}
    		        mycam.startPreview();
    				isFlashOn = true;
    				tt.setImageResource(R.drawable.flash_button_on);
    			}
    		}
    	});
    }
    
    public void screenTorchOn(){
    	ImageButton sb = (ImageButton)findViewById(R.id.sb);
    	sb.setOnClickListener(new OnClickListener(){
    		@Override
    		public void onClick(View v){
    			Intent scIntent = new Intent(MainActivity.this, ScreenTorch.class);
    			MainActivity.this.startActivity(scIntent);
    		}
    	});
    }
    
    //Function to Disable Sleep Mode
    private void disableSleepMode(){
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    
    @Override
    public void onDestroy(){
    	super.onDestroy();
    	if (mycam != null){
			isFlashOn = false;
    		mycam.stopPreview();
			mycam.release();
			mycam = null;
    	}
    }
    
    @Override
    public void surfaceChanged(SurfaceHolder holder, int I, int J, int K) {
      //Pass
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
      //try {
      //  mycam.setPreviewDisplay(holder);
      //} catch (IOException e) {
      //  e.printStackTrace();
      //}
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu, menu);
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch (item.getItemId()){
    		case R.id.menu_item_about:
    			Intent abIntent = new Intent(MainActivity.this, AboutApp.class);
    			MainActivity.this.startActivity(abIntent);
    			return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }
}