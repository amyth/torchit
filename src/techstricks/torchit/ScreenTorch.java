package techstricks.torchit;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class ScreenTorch extends Activity{
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set Activity to Full Screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        						WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Set the Layout
        setContentView(R.layout.screen_torch);
        //Set BrightNess
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.screenBrightness = 1F;
        getWindow().setAttributes(params);
        screenTorchOff();
    }
	
	public void screenTorchOff(){
    	ImageButton pw = (ImageButton)findViewById(R.id.pw);
    	pw.setOnClickListener(new OnClickListener(){
    		@Override
    		public void onClick(View v){
    			// Set Brightness Back to Normal / Auto Mode
    			WindowManager.LayoutParams params = getWindow().getAttributes();
    	        params.screenBrightness = -1F;
    	        getWindow().setAttributes(params);
    	        // Kill Activity
    			finish();
    		}
    	});
    }
}